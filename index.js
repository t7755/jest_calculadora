class Calculator {

    sum(a, b) {
      return a + b;
    }
  
    divide(a, b) {
      if (b === 0) {
        throw new Error('Invalid argument');
      }
  
      return a / b;
    }

    multiply(a, b) {
        return a * b;
    }

    subtract(a, b) {
        return a - b;
    }

    example(a,b){
        return 0;
    }


  }
  
  module.exports = Calculator;